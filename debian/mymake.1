.\"                                      Hey, EMACS: -*- nroff -*-
.\" (C) Copyright 2021 Filip Strömbäck <filip@fprg.se>,
.\"
.\" First parameter, NAME, should be all caps
.\" Second parameter, SECTION, should be 1-8, maybe w/ subsection
.\" other parameters are allowed: see man(7), man(1)
.TH MYMAKE 1 "June 22 2021"
.\" Please adjust this date whenever revising the manpage.
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp <n>    insert n+1 empty lines
.\" .RS        indent, for code examples
.\" .RE        decrease relative indent
.\" for manpage-specific macros, see man(7)
.SH NAME
mymake - simple build system for C/C++
.SH SYNOPSIS
.B mymake
[\-?] [\-f] [\-c] [\-t] [\-e] [\-ne] [\-p \fIpath\fR] [\-j \fIprocesses\fR] [\-o \fIoutput\fR] [\-d \fIlevel\fR] [\-\-default\-input \fIinput\fR] [\fIfiles\fR]... [\fIoptions\fR]... [\-\- \fIargs...\fR]
.br
.B mymake
\-\-config
.br
.B mymake
\-\-target
.br
.B mymake
\-\-project
.br
.SH DESCRIPTION
The
.B mymake
program builds C/C++ programs. It aims to be able to build simple
programs (referred to as \fItargets\fR) with close to zero
configuration, yet be powerful enough to handle larger programs as the
initially simple project grows.
.PP
Once installed, run
.sp 1
.RS
.B mymake \-\-config
.RE
.sp 1
to generate the global configuration file for the current user. This
file specifies how to compile C/C++ programs on the current system,
and defines the default parameters that can be used in other files as
described below. The default contents are typically sufficient.
.PP
After the initial configuration, a simple program can be compiled by
navigating to the directory the source code of the program and typing
.sp 1
.RS
.B mymake myprogram.cpp
.RE
.sp 1
This will compile and link the program using the configuration in the
global configuration, and if compilation is successful also execute
the program. Command-line parameters may be passed to the program
using the \fI\-\-\fR or the \fI\-a\fR parameter
.sp 1
.RS
.B mymake myprogram.cpp \-\- param1 param2...
.RE
.sp 1
The default behavior also works with programs consisting of multiple
files. \fBmymake\fR examines which files are included in the source file
specified on the command-line and compiles those files as well. This
is based on the assumption that if the file \fButils.h\fR is included,
then the file \fButils.cpp\fR should also be compiled (if it
exists). Of course, different extensions than \fB.cpp\fR are also
examined. As is typically the case with other build systems, only
files changed since the last build are rebuilt.
.PP
If the program being compiled does not follow the assumption that
header files and implementation files appear pairwise, it is possible
to create a file called \fB.mymake\fR in the project directory and add
the following to it
.sp 1
.RS
.B []
.br
.B input=*
.RE
.sp 1
This instructs mymake to include all implementation files in the
compilation, regardless of whether or not they are considered to be
needed by the program or not. It is also possible to add a particular
file to the input statement (e.g. \fBmyprogram.cpp\fR). In both cases,
it will no longer be necessary to specify a file on the command line.
.PP
As of version 2.3.0, mymake tracks the command-lines used to compile
each file, and re-compiles source files where the command line has
changed. This means that mymake is able to re-compile the relevant
parts of the project if the configuration file has changed.
.PP
As the project grows, it is also possible to divide the project into
separate sub-projects and have mymake manage dependencies
automatically. This is done by creating a \fB.myproject\fR file in the
root directory of the project. This tells mymake to treat all
directories as sub-projects, each which may contain their own
\fB.mymake\fR file. See CONFIGURATION for details.
.SH OPTIONS
The usual GNU command line syntax is respected, with long options
starting with two dashes (`\-'). Long options are equivalent to short
options.
.TP
.B \-?, \-\-help
Print a message outlining basic usage and command-line options.
.TP
.B \-f, \-\-force
Force re-compilation of all required files, even files that would
otherwise be considered to be up-to-date. This is useful if the
configuration files were changed, for example.
.TP
.B \-c, \-\-clean
Clean build files by removing the \fBexecDir\fR and \fBbuildDir\fR
indicated by the configuration based on any options specified.
.TP
.B \-t, \-\-time
Output information about the wall-clock time used in each step
of compilation.
.TP
.B \-e, \-\-execute
Execute the executable after successfully building a target. The
default configuration executes the program, but this behavior may be
overridden by project-specific configuration files. The command-line
parameters overrides all configuration files.
.TP
.B \-ne, \-\-not \-\-execute
Do not execute the executable after a successful build. The inverse of
\fI\-f\fR.
.TP
.B \-p, \-\-exec\-path
Specify the working directory when running the compiled
executable. The default value is the path of the project to which the
executable belongs.
.TP
.B \-j \fIprocesses\fR, \-\-threads \fIprocesses\fR
Spawn up to the specified number of processes in parallel during
compilation. This typically means that compilation may use up to the
specified number of hardware threads. This is typically not needed, as
the user's preference is specified in the global configuration file.
.TP
.B \-o \fIoutput\fR, \-\-output \fIoutput\fR
Specify the name of the output file (note: not the location). This can
be used to override the default behavior, but this is better done in
one of the configuration files.
.TP
.B \-d \fIlevel\fR, \-\-debug \fIlevel\fR
Specify the debug level, either as a number or as the associated
label. The following levels are available:
.RS
.TP
.B 0: QUIET
No output except for fatal errors.
.TP
.B 1: NORMAL
The default value. Outputs progress information.
.TP
.B 2: PEDANTIC
Warns about configuration issues that might be errors. Useful as a
first step in debugging configurations.
.TP
.B 3: INFO
Prints information about decisions during the build. Good for
debugging configurations.
.TP
.B 4: VERBOSE
All information you will typically need when debugging configuration
issues.
.TP
.B 5: DEBUG
Information typically only needed when debugging mymake itself.
.RE
.TP
.B \-\-default\-input \fIinput\fR
Add \fIinput\fR as an input file if no other inputs were specified
either on the command line or in any configuration files. This option
is intended to be used when integrating mymake in an editor. The
editor may then always supply the name of the current open file as a
\fB\-\-default\-input\fR to make mymake do the right thing based on
configurations: compiling the current file if nothing else is
specified, otherwise follow the other, more precise instructions in
the configuration files.
.TP
.B [options]
Zero or more options may also be specified on the command line. These
options may correspond to sections in one of the configuration files
to enable (or disable). See the CONFIGURATION section below for
details.
.TP
.B [files]
Files may also be specified on the command line. Mymake will add all
names that correspond to existing files (possibly first appending
known file extensions) to the list of input files to process.
.TP
.B \-\-config
Create the global configuration in \fB~/.mymake\fR. This can also be
used to revert the global configuration to the default state. Will ask
for the preferred number of threads to use during compilation.
.TP
.B \-\-target
Creates the file \fB.mymake\fR in the current directory. It fills the
file with a template configuration that contains common options and
settings.
.TP
.B \-\-project
Creates the file \fB.myproject\fR in the current directory. The file
contains a template suitable for a project, i.e. a directory
consisting of multiple targets.
.SH CONFIGURATION
The configuration in mymake consists of a set of named variables, each
of which contain an array of strings. The value of these variables may
originate from one (or more) of four locations: the global
configuration (\fB~/.mymake\fR), the project configuration
(\fB.myproject\fR, if present), the target configuration
(\fB.mymake\fR, if present), or the command-line. The project and
target configurations may reside in the current directory, or any
parent directories. If a project configuration is found, target
configurations are expected to be located in sub-directories to the
project configuration. Options are always applied in the order
specified above. This means that options in the global configuration
may be overridden by other configurations, and that the command-line
parameters are always applied last. The exception is when compiling a
project. Then, command line parameters are only applied when resolving
the project configuration, not when resolving the configuration for
individual targets.
.PP
All configuration files follow the same format. They all consist of a
sequence of assignments to variables, optionally grouped into zero or
more sections. Lines starting with `#' are comments. Assignments have
one of the following two forms:
.sp 1
.RS
name=value
.br
name+=value
.RE
.sp 1
The first form replaces the entire contents of the variable with the
string \fIvalue\fR, while the second adds \fIvalue\fR as the last
element of the array. As a special case, if the first form is used,
and \fIvalue\fR is empty, the variable is assigned the empty
array. Assignments may be grouped into sections by adding section
headers before them. A section header has the following form:
.sp 1
.RS
[\fIoption1\fR,\fIoption2\fR,...]
.RE
.sp 1
The meaning of the section header is: only consider the following
assignments if all options that appear in the header are present in
the context in which the configuration is evaluated (e.g. specified on
the command line). Thus, the header [] is always evaluated, [release]
is only evaluated if the \fIrelease\fR option has been specified, and
[release,unix] only if both \fIrelease\fR and \fIunix\fR have been
specified. Additionally, an option may be prefixed with an `!' to mean
that the particular option has to be absent, for example:
[!release,unix].
.PP
Each configuration file is then evaluated by mymake in turn to provide
the set of variables to use during compilation. This is done by
evaluating each assignment in each file in the order they are
specified, ignoring any sections that should be skipped according to
the available options. Typically, each file is only evaluated once,
with a context consisting of the options specified on the
command-line. The exception is \fI.myproject\fR-files, which are
covered in the PROJECTS section below.
.SH PRE-DEFINED OPTIONS
The following options are pre-defined by mymake or the default
configuration, and can be used by default:
.TP
.B release
Produce a release version of the program. This typically means turning
on more aggressive optimizations.
.TP
.B lib
Produces a static library. Typically used in projects when some
targets are static libraries used by other targets in the project.
.TP
.B sharedlibe
Produces a dynamic library. Typically used in projects, like
\fIlib\fR.
.TP
.B unix
Defined by mymake when compiling for a UNIX-like system.
.TP
.B windows
Defined by mymake when compiling for a Windows-like system.
.TP
.B project
Defined automatically when evaluating the \fI.myproject\fR file in the
project context.
.TP
.B build
Defined automatically when evaluating the \fI.myproject\fR file to
find options for the targets in a project.
.TP
.B deps
Defined automatically when evaluating the \fI.myproject\fR file to
find explicit dependencies between projects.
.SH VARIABLES
The following variables are used by mymake to define what should be
done. Some of these variables are treated specially by mymake itself,
others are just defined by the global configuration. It is possible to
define and use other variables in configuration files.
.TP
.B ext
Array of the file extensions you want to compile. Whenever mymake
realizes you have included \fIx.h\fR, looks for all extensions in
\fBext\fR and tries them to find the corresponding implementation
file.
.TP
.B execExt
File extension of executable files. Added to the output filename
automatically.
.TP
.B intermediateExt
File extension of intermediate files. Typically \fB.o\fR on UNIX systems.
.TP
.B buildDir
String containing the directory used to store all temporary files when
building your program.  Relative to the root directory of the target
(i.e. where the \fI.mymake\fR file is).
.TP
.B execDir
String containing the directory used to store the final output (the
executable) of all targets. Relative to the root directory of the
target.
.TP
.B ignore
Array of wildcard patterns (like in the shell) that determines if a
certain file should be ignored. Useful when working with templates
sometimes, or when parts of the source code should not be compiled.
.TP
.B noIncludes
Array of wildcard patterns (like in the shell) that determines if a
certain path should not be scanned for headers. Useful when you want
to parts of the code that is not C/C++, where it is not meaningful to
look for \fI#include\fR.
.TP
.B input
Array of file names to use as roots when looking for files that needs
to be compiled. Anything that is not an option that is specified on
the command line is appended to this variable. The special value \fI*\fR
can be used to indicate that all files with an extension in the \fIext\fR
variable should be compiled. This is usually what you want when you
are compiling a library of some kind.
.TP
.B output
String specifying the name of the output file. If not specified, the
name of the first input file is used instead.
.TP
.B appendExt
Append the original extension of the original source file to the
intermediate file when compiling. This allows mymake to compile
projects where there are multiple files with the same name,
e.g. \fIfoo.cpp\fR and \fIfoo.c\fR without both trying to create
\fIfoo.o\fR and thereby causing compilation to fail. Mymake warns you
if you might need to add use this option.
.TP
.B include
Array of paths that should be added to the include path of the compilation.
.TP
.B includeCl
Flag to prepend all elements in \fIinclude\fR.
.TP
.B includes
Generated automatically by mymake, equivalent to adding the contents
of \fIincludeCl\fR before each element in \fIinclude\fR.
.TP
.B library
Array of system libraries that should be linked to your executable.
.TP
.B libraryCl
Flag to prepend all elements in \fIlibrary\fR.
.TP
.B localLibrary
Array of local libraries that should be linked to your executable
(usually used in a project).
.TP
.B localLibraryCl
Flag to prepend all elements in \fIlocalLibrary\fR.
.TP
.B libs
Automatically generated by mymake, equivalent to adding
\fIlibraryCl\fR before all elements of \fIlibrary\fR, also including
local libraries.
.TP
.B define
Preprocessor defines.
.TP
.B defineCl
Preprocessor define flag.
.TP
.B exceute
Yes or no, telling if mymake should execute the program after a
successful compilation. This can be overridden on the command line
using \fI\-e\fR or \fI\-ne\fR.
.TP
.B pch
The precompiled header file name that should be used. If you are using
the default configuration, you only need to set this variable to use
precompiled headers. If you are using \fI#pragma once\fR in gcc, you
will sadly get a warning that seems impossible to disable (it is not a
problem when precompiling headers).
.TP
.B pchFile
The name of the compiled version of the file in \fIpch\fR.
.TP
.B pchCompile
Command line for compiling the precompiled header file.
.TP
.B pchCompileCombined
If set to yes, \fIpchCompile\fR is expected to generate both the
pch-file and compile a .cpp-file.
.TP
.B preBuild
Array of command-lines that should be executed before the build is
started. Expands variables.
.TP
.B preBuildCreates
Array of files created by the pre-build step which should also be
included in the compilation.  These are expected not to introduce any
additional dependencies into the project, as they are not available at
the point where mymake resolves dependencies between files and
targets.
.TP
.B postBuild
Array of command-lines that should be executed after the build is
completed. Expands variables.
.TP
.B compile
Array of command lines to use when compiling files. Each command line
starts with a pattern (ending in \fI:\fR) that is matched against the
file name to be compiled. The command line added last is checked
first, and the first matching command-line is used. Therefore it is
useful to first add the general command-line (starting with \fI*:\fR),
and then add more specific ones. Here, you can use \fI<file>\fR for
the input file and \fI<output>\fR.
.TP
.B link
Command line used when linking the intermediate files. Use
\fI<files>\fR for all input files and \fI<output>\fR for the output
file-name.
.TP
.B linkOutput
Link the output of one target to any target that are dependent on that
target. See projects for more information.
.TP
.B forwardDeps
Forward any of this target's dependencies to any target that is
dependent on this target.
.TP
.B env
Set environment variables. Each of the elements in \fIenv\fR are
expected to be of the form: \fIvariable=value\fR or
\fIvariable<=value\fR or \fIvariable=>value\fR. The first form
replaces the environment variable \fIvariable\fR with \fIvalue\fR, the
second form prepends \fIvalue\fR to \fIvariable\fR using the system's
separator (\fI:\fR on unix and \fI;\fR on windows), the third form
appends \fIvalue\fR to \fIvariable\fR. The second and third forms are
convenient when working with \FIPATH\FR for example.
.TP
.B explicitTargets
In projects: ignore any potential targets that do not have their own
\fI.mymake\fR-file.
.TP
.B parallel
In projects, this indicates if projects that have all dependencies
satisfied may be built in parallel. The default value is \fIyes\fR, so
projects not tolerating parallel builds may set it to \fIno\fR.  In
targets, this indicates if files in targets may be built in
parallel. If so, all input files, except precompiled headers, are
built in parallel using up to \fImaxThreads\fR threads globally. If
specific targets do not tolerate this, set \fIparallel\fR to \fIno\fR,
and mymake will build those targets in serial.
.TP
.B maxThreads
Limits the global number of threads (actually processes) used to build
the project/target globally.
.TP
.B usePrefix
When building in parallel, add a prefix to the output corresponding to
different targets. Defaults to either \fIvc\fR or \fIgnu\fR (depending
on your system). If you set it to \fIno\fR, no prefix is
added. \fIvc\fR adds \fIn>\fR before output, \fIgnu\fR adds \fIpn: \fR
before output. This is so that Emacs recognizes the error messages
from the vc and the gnu compiler, respectively.
.TP
.B absolutePath
Send absolute paths to the compiler, this helps emacs find proper
source files in projects with multiple targets.
.TP
.B implicitDeps
(defaults to \fIyes\fR), if set, mymake tries to figure out
dependencies between targets by looking at includes.  Sometimes, this
results in unneeded circular dependencies, causing compilation to
fail, so sometimes it is neccessary to set this to \fIno\fR.
.PP
All variables that contain commands to execute can be prefixed with
an integer followed by a colon (for example \fI1:gcc\fR). If prefixed
with the integer N, the first N lines of output from that command
are removed from the output. As such, this can be used to ignore
status information from commands during the build, without ignoring
useful information. For example, the C++ compiler on Windows
outputs a line containing the filename being compiled by default.
This is unnecessary as mymake already does this. It can thus be
suppressed by issuing the command \fI1:cl ...\fR.
.PP
This feature is possible to use in the variables \fIcompile\fR,
\fIpchCompile\fR, and \fIlink\fR.
.SH VARIABLES IN STRINGS
When mymake uses some variables (most notably, the compilation and
link command lines) it looks at each string and recursively replaces
any variables that appear there. Note that this is \fBnot\fR done when
the configuration is evaluated, only when the variables are actually
used.
.PP
Any occurrences of \fB<variable>\fR are replaced with the contents of
\fBvariable\fR. It is also possible to prepend a string to each
element in another variable using the syntax \fB<prefix*variable>\fR,
which means that the string in the variable \fBprefix\fR is prepended
to each element in the variable \fBvariable\fR.
.PP
The special variable \fB<env:X>\fR (where \fBX\fR is any string) can
be used to extract the value of environment variables. This is useful
to, for example, inspect the value of \fBCFLAGS\fR during compilation.
The benefit of using this syntax over relying on shell expansion is
that it allows mymake to track changes in the environment variables.
.PP
It is also possible to perform an operation on each element in the
array using the syntax \fB<op|variable>\fR. It is also possible to
both perform an operation and prepend data using
\fB<prefix*op|variable>\fR. Supported operations are:
.TP
.B title
Treat the element as a path and extract the file or directory name
(e.g. \fIsrc/foo.txt\fR gives \fIfoo.txt\fR).
.TP
.B titleNoExt
Same as \fItitle\fR, but the file extension is removed as well.
.TP
.B noExt
Remove the file extension from a path.
.TP
.B path
Format the element as a path for the current operating system. For
example \fIsrc/foo.txt\fR evaluates to \fIsrc\\foo.txt\fR on Windows.
.TP
.B buildpath
Make the element into a path inside the build path.
.TP
.B execpath
Make the element into a path inside the executable path.
.TP
.B parent
Evaluates to the parent directory of the path. If no parent is given
(e.g. only a file name), the element is removed from the array.
.TP
.B if
Make all elements empty. This can be used to test if a variable
contains a value and then include some other text. For example
\fI<usePch*if|pchFile>\fR to add the flag inside \fIusePch\fR if a
file is specified in \fIpchFile\fR.
.SH PROJECTS
A project is a collection of targets linked together by a
\fI.myproject\fR-file. The \fI.myproject\fR file is evaluated multiple
times with different options present to extract information about the
project:
.PP
.B build:
This option is specified during one evaluation to extract a list of
options to apply to each of the sub-projects. Thus, a project file
typically contains a section as follows:
.sp 1
.RS
[build]
.br
main+=debug
.br
libfoo+=lib
.br
libfoo+=debug
.RE
.sp 1
In this case, we instruct mymake to build the \fImain\fR target with
the \fIdebug\fR option present, and the target \fIlibfoo\fR with the
options \fIdebug\fR and \fIlib\fR. There is also a special target,
\fIall\fR, which options will apply to all targets in the project.
.PP
.B deps:
This option is specified during one evaluation to extract explicit
dependencies between projects. By default, mymake finds dependences
between projects automatically by examining includes across
projects. In certain cases it is, however, useful to introduce extra
dependencies to ensure that some dynamically loaded parts are also
built. This section is very similar to the build section. Mymake
expects to find variables corresponding to each target, and that these
variables contains names of other targets.
.PP
If one target results in a library, it is convenient to set the
variable \fIlinkOutput\fR to yes for that target. Mymake will then add
the output of the library target to the \fIlibrary\fR variable of any
targets that depend on it.
.PP
After mymake has extracted the necessary project information, the
\fI.myproject\fR-file is also evaluated once for each target. Thus, it
is possible to specify additional variables or options that apply to
all targets in the project in the \fI.myproject\fR file.
